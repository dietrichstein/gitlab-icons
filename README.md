# GitLab Icons

Need an an icon for something GitLab related? The best place for high-quality logo sources is the official [GitLab Press Kit](https://about.gitlab.com/press/press-kit/#logos). However, perhaps you're looking for ready-to-use icons instead? If so, feel free to use the ones provided here.

Please note, the colors used in the icons provided here are optimized to provide adequate contrast at small sizes with dark background. You may wish to download the SVG files yourself to further tune the colors for your use case.

## Solid Style Icons

|Color                     |Primary|Secondary|Tertiary|SVG|PNG|
|--------------------------|-------|---------|--------|---|---|
|Red                       |`#ff6f89`|`#eb2624`|`#c13524`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-red.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-red-16.png">|
|Orange (Brand<sup>*</sup>)|`#fca121`|`#fc6d26`|`#e24329`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-orange.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-orange-16.png">|
|Yellow                    |`#ffe887`|`#ebd224`|`#c1a71d`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-yellow.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-yellow-16.png">|
|Green                     |`#a7ff91`|`#25eb24`|`#24c11d`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-green.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-green-16.png">|
|Cyan                      |`#9fffe3`|`#00eac5`|`#00c4a3`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-cyan.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-cyan-16.png">|
|Blue                      |`#5fdfff`|`#42b4ff`|`#1d86c1`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-blue.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-purple-16.png">|
|Gray                      |`#c1c1c1`|`#919191`|`#7e7e7e`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-solid-gray.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-solid-gray-16.png">|

> <sup>*</sup> The "tertiary" color listed for "Orange (Brand)" above is taken from the official SVG. The press page lists the actual color as `#db3b21` but they are very similar.

## Outline Style Icons

|Color            |Primary|SVG|PNG|
|-----------------|---------|---|---|
|Black            |`#000000`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-black.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-black-16.png">|
|Gray             |`#cccccc`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-gray.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-gray-16.png">|
|White            |`#ffffff`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-white.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-white-16.png">|
|Red              |`#ff6f89`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-red.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-red-16.png">|
|Orange           |`#fca121`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-orange.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-orange-16.png">|
|Yellow           |`#ffe887`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-yellow.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-yellow-16.png">|
|Green            |`#a7ff91`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-green.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-green-16.png">|
|Cyan             |`#9fffe3`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-cyan.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-cyan-16.png">|
|Blue             |`#5fdfff`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-blue.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-blue-16.png">|
|Purple           |`#9a89dd`|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/svg/gitlab-outline-purple.svg">|<img width="16" src="https://gitlab.com/dietrichstein/gitlab-icons/raw/master/png/gitlab-outline-purple-16.png">|
